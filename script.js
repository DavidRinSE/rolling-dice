let totalDice = parseInt(document.getElementById("totalDice").value)
let minimum = totalDice
let maximum = totalDice*6
function roll(){
    let diceCount = {}
    let maxWidth = 0;
    
    for (let roll = 0; roll < 1000; roll++){
        let rollTotal = 0;
        for(let dice = 0; dice < totalDice; dice++){
            rollTotal += Math.floor(Math.random() * (6) + 1)
        }
        if(isNaN(diceCount[rollTotal])){
            diceCount[rollTotal] = 1
        } else {
            diceCount[rollTotal] += 1
        }
        if(diceCount[rollTotal] > maxWidth){
            maxWidth = diceCount[rollTotal]
        }
    }

    for(let i = minimum; i < maximum+1; i++){
        if(diceCount[i] == null){
            diceCount[i] = 0
        }
    }
    return {diceCount: diceCount, maxWidth: maxWidth}
}
function getGraph(roll, title){
    let {diceCount, maxWidth} = roll
    let graphWrap = document.createElement("div")
    graphWrap.className = "graphWrap"
    let graphWrapTitle = document.createElement("h1")
    graphWrapTitle.appendChild(document.createTextNode(title))
    graphWrap.appendChild(graphWrapTitle)
    
    let graph = document.createElement("div")
    graph.className = "graph"

    for (let i = minimum; i < Object.keys(diceCount).length + minimum; i++){
        let graphItemWrap = document.createElement('div')
        graphItemWrap.className = "graphItemWrap"

        let graphItem = document.createElement('div')
        graphItem.className = "graphItem"

        let width = (diceCount[i]/(maxWidth + 10))*55
        graphItem.setAttribute('style', 'width: ' + Math.floor(width) + 'vw')
        graphItemWrap.appendChild(graphItem)

        if(width < 9){
            graphItemWrap.appendChild(document.createTextNode(i + ": " + diceCount[i]))
        } else {
            graphItem.appendChild(document.createTextNode(i + ": " + diceCount[i]))
        }
        if(i === minimum){
            graphItem.id = "first"
        }else if (i === maximum) {
            graphItem.id = "last"
        }
        graph.appendChild(graphItemWrap)
    }
    graphWrap.appendChild(graph)
    return graphWrap
}
function getSampleDOM(roll){
    let { diceCount, maxWidth } = roll
    let wrapper = document.createElement('div')
    wrapper.className = "content"
    
    let rollsWrap = document.createElement('div')
    rollsWrap.className = "rollsWrap"
    
    for (let i = minimum; i < Object.keys(diceCount).length + minimum; i++){
        let roll = document.createElement('div')
        roll.className = "roll"
        roll.appendChild(document.createTextNode(i + ": " + diceCount[i]))
        rollsWrap.appendChild(roll)
    }
    wrapper.appendChild(rollsWrap)
    wrapper.appendChild(getGraph(roll, "Sample Roll"))
    return wrapper
}
function getPopulationDOM(population){
    let {diceCount, maxWidth} = population
    let wrapper = document.createElement('div')
    wrapper.className = "content"
    wrapper.appendChild(getGraph(population, "Population"))

    let rolls = []
    for(let i = minimum; i < Object.keys(diceCount).length + minimum; i++){
        for(let x = 0; x < diceCount[i]; x++){
            rolls.push(i)
        }
    }

    let sumFunc = (total, currentValue) => total + currentValue;
    let sum = rolls.reduce(sumFunc)
    let average = (sum/rolls.length).toFixed(2)
    let analysis = {sum: sum, average: average}

    let analysisWrap = document.createElement('div')
    analysisWrap.className = "analysisWrap"
    for(let i = 0; i < Object.keys(analysis).length; i++){
        let div = document.createElement('div')
        div.className = "graphAnalysis"
        div.appendChild(document.createTextNode(Object.keys(analysis)[i].charAt(0).toUpperCase() + Object.keys(analysis)[i].slice(1) + ": " + analysis[Object.keys(analysis)[i]]))
        analysisWrap.appendChild(div)
    }
    wrapper.appendChild(analysisWrap)
    return wrapper
}
function updatePopulation(population, roll) {
    let {diceCount, maxWidth} = roll
    let popDiceCount = population.diceCount
    let popMaxWidth = population.maxWidth
    
    for(let i = minimum; i < Object.keys(popDiceCount).length + minimum; i++){
        popDiceCount[i] += diceCount[i]
        if (popDiceCount[i] > popMaxWidth){
            popMaxWidth = popDiceCount[i]
        }
    }

    let newPopulation = {diceCount: popDiceCount, maxWidth: popMaxWidth}
    return newPopulation
}

let currentRoll = roll()
let population = currentRoll

let oldSample;
let newSample = getSampleDOM(currentRoll)

let oldPopulation;
let newPopulation = getPopulationDOM(population)

let root = document.getElementById('root')
root.appendChild(newSample)
root.appendChild(newPopulation)

function reset() {
    if(isNaN(parseInt(document.getElementById('totalDice').value))) {
        alert("Nice try! Give me a number")
    } else {
        totalDice = parseInt(document.getElementById("totalDice").value)
        minimum = totalDice
        maximum = totalDice*6

        currentRoll = roll()
        population = currentRoll

        oldSample = newSample
        newSample = getSampleDOM(currentRoll)

        oldPopulation = newPopulation
        newPopulation = getPopulationDOM(population)

        root.replaceChild(newSample, oldSample)
        root.replaceChild(newPopulation, oldPopulation)
    }
}

setInterval(function(){
    currentRoll = roll()
    population = updatePopulation(population, currentRoll)

    oldSample = newSample
    newSample = getSampleDOM(currentRoll)

    oldPopulation = newPopulation
    newPopulation = getPopulationDOM(population)

    root.replaceChild(newSample, oldSample)
    root.replaceChild(newPopulation, oldPopulation)
}, 3000)